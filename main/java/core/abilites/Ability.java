package core.abilites;

public interface Ability {

    byte getScore();
    byte getModifier();
    byte getTempAdjustment();
    byte getTempModifier();
}
