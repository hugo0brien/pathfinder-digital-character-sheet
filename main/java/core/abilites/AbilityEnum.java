package core.abilites;

public enum AbilityEnum {
    STRENGHT,
    DEXTERITY,
    CONSTITUTION,
    INTELLIGENCE,
    WISDOM,
    CHARISMA
}
