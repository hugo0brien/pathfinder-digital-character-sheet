package core.abilites;

public class AbilityImpl implements Ability {

    private byte score;
    private byte modifier;
    private byte tempAdjustment;
    private byte tempModifier;

    public AbilityImpl(int score) {
        setScore((byte) score);
    }

    private byte calcModifierValue(int score) {
        return (byte) Math.floor((float) (score - 10) / 2);
    }

    @Override
    public byte getScore() {
        return score;
    }

    @Override
    public byte getModifier() {
        return modifier;
    }

    @Override
    public byte getTempAdjustment() {
        return tempAdjustment;
    }

    @Override
    public byte getTempModifier() {
        return tempModifier;
    }

    public void setScore(byte score) {
        this.score = score;
        this.modifier = calcModifierValue(score);
    }

    public void setTempAdjustment(byte tempAdjustment) {
        this.tempAdjustment = tempAdjustment;
        this.tempModifier = calcModifierValue(tempAdjustment);
    }
}
