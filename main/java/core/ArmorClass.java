package core;

public class ArmorClass implements Calculable {

    private byte total;
    private byte armorBonus;
    private byte shieldBonus;
    private byte dexModifier;
    private byte sizeModifier;
    private byte naturalArmor;
    private byte deflectionModifier;
    private byte miscModifier;

    public ArmorClass(byte armorBonus, byte shieldBonus, byte dexModifier, byte sizeModifier, byte naturalArmor, byte deflectionModifier, byte miscModifier) {
        this.armorBonus = armorBonus;
        this.shieldBonus = shieldBonus;
        this.dexModifier = dexModifier;
        this.sizeModifier = sizeModifier;
        this.naturalArmor = naturalArmor;
        this.deflectionModifier = deflectionModifier;
        this.miscModifier = miscModifier;
        recalcTotal();
    }

    @Override
    public void recalcTotal() {
        total = (byte) (10 + armorBonus + shieldBonus + dexModifier + sizeModifier + naturalArmor + deflectionModifier + miscModifier);
    }

    @Override
    public Object getTotal() {
        return total;
    }

    public void setArmorBonus(byte armorBonus) {
        this.armorBonus = armorBonus;
        recalcTotal();
    }

    public void setShieldBonus(byte shieldBonus) {
        this.shieldBonus = shieldBonus;
        recalcTotal();
    }

    public void setDexModifier(byte dexModifier) {
        this.dexModifier = dexModifier;
        recalcTotal();
    }

    public void setSizeModifier(byte sizeModifier) {
        this.sizeModifier = sizeModifier;
        recalcTotal();
    }

    public void setNaturalArmor(byte naturalArmor) {
        this.naturalArmor = naturalArmor;
        recalcTotal();
    }

    public void setDeflectionModifier(byte deflectionModifier) {
        this.deflectionModifier = deflectionModifier;
        recalcTotal();
    }

    public void setMiscModifier(byte miscModifier) {
        this.miscModifier = miscModifier;
    }
}
