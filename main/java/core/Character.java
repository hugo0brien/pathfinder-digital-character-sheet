package core;

import core.abilites.Ability;
import core.abilites.AbilityEnum;
import core.enums.Alignment;
import core.enums.CreatureSize;
import core.enums.Deity;
import core.enums.Gender;
import core.speed.Speed;
import core.speed.SpeedEnum;

import java.util.HashMap;
import java.util.Map;

public class Character {

    private String characterName;
    private Alignment alignment;
    private Player player;

    private byte level;
    private Deity deity;
    private String homeland;

    private Race race;
    private CreatureSize size;
    private Gender gender;
    private int age;
    private int height;
    private int weight;
    private String hair;
    private String eyes;

    private byte healthPoint;
    private byte damageReduced;
    private byte currentHP;
    private byte nonlethalDamage;

    private Map<AbilityEnum, Ability> abilityMap = new HashMap<>();
    private Map<SpeedEnum, Speed> speedMap = new HashMap<>();

    private static final String STRENGTH = "strength";
    private static final String DEXTERITY = "dexterity";
    private static final String CONSTITUTION = "constitution";
    private static final String INTELLIGENCE = "intelligence";
    private static final String WISDOM = "wisdom";
    private static final String CHARISMA = "charisma";


}
