package core;

public class Initiative implements Calculable {

    private byte total;
    private byte dexModifier;
    private byte miscModifier;

    public Initiative(int dexModifier, int miscModifier) {
        this.dexModifier = (byte) dexModifier;
        this.miscModifier = (byte) miscModifier;
    }

    public void setDexModifier(byte dexModifier) {
        this.dexModifier = dexModifier;
        recalcTotal();
    }

    public void setMiscModifier(byte miscModifier) {
        this.miscModifier = miscModifier;
        recalcTotal();
    }

    @Override
    public Object getTotal() {
        return total;
    }

    @Override
    public void recalcTotal() {
        total = (byte) (dexModifier + miscModifier);
    }
}
