package core.speed;

public class Speed {
    private byte feet;
    private byte cells;

    public Speed(byte feet, byte cells) {
        this.feet = feet;
        this.cells = cells;
    }

    public byte getFeet() {
        return feet;
    }

    public void setFeet(byte feet) {
        this.feet = feet;
    }

    public byte getCells() {
        return cells;
    }

    public void setCells(byte cells) {
        this.cells = cells;
    }
}
