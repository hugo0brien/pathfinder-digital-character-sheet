package core.speed;

public enum SpeedEnum {
    BASE_SPEED,
    WITH_ARMOR,
    FLY,
    SWIM,
    CLIMB,
    BURROW
}
