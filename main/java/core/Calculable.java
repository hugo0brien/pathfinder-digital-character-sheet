package core;

public interface Calculable<T> {

    void recalcTotal();
    T getTotal();
}
