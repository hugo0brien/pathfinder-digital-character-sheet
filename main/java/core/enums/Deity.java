package core.enums;

import core.enums.Alignment;

public enum Deity {

    ABADAR("Abadar", Alignment.LAWFUL_NEUTRAL),
    ASMODEUS("Asmodeus", Alignment.LAWFUL_EVIL),
    CALISTRIA("Calistria", Alignment.CHAOTIC_NEUTRAL),
    CAYDEN_CAILEAN("Cayder Cailean", Alignment.CHAOTIC_GOOD),
    DESNA("Desna", Alignment.CHAOTIC_GOOD),
    ERASTIL("Erastil", Alignment.LAWFUL_GOOD),
    GOZREH("Gozreh", Alignment.TRUE_NEUTRAL),
    GORUM("Gorum", Alignment.CHAOTIC_NEUTRAL),
    ZON_KUTHON("Zon-Kuthon", Alignment.LAWFUL_EVIL),
    IRORI("Irori", Alignment.LAWFUL_NEUTRAL),
    IOMEDAI("Iomedae", Alignment.LAWFUL_GOOD),
    LAMASHTU("Lamashtu", Alignment.CHAOTIC_EVIL),
    NETHYS("Nethys", Alignment.TRUE_NEUTRAL),
    NORGOBER("Norgober", Alignment.NEUTRAL_EVIL),
    ROVAGUG("Rovagug", Alignment.CHAOTIC_EVIL),
    SARENRAE("Sarenrae", Alignment.NEUTRAL_GOOD),
    TORAG("Torag", Alignment.LAWFUL_GOOD),
    URGATHOA("Urgathoa", Alignment.NEUTRAL_EVIL),
    PHARASMA("Pharasma", Alignment.TRUE_NEUTRAL),
    SHELYN("Shelyn", Alignment.NEUTRAL_GOOD),

    ;

    private String name;
    private Alignment alignment;

    Deity(String name, Alignment alignment) {
        this.name = name;
        this.alignment = alignment;
    }

    public String getName() {
        return name;
    }

    public Alignment getAlignment() {
        return alignment;
    }
}
