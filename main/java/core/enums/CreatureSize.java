package core.enums;

public enum CreatureSize {

    FINE(8, -8, 8, 16, 0.2f, 0),
    DIMINUTIVE(4, -4, 6, 12, 1f, 0),
    TINY(2, -2, 4, 8, 1.5f, 0),
    SMALL(1, -1, 2, 4, 5f, 5),
    MEDIUM(0, 0, 0, 0, 5f, 5),
    LARGE_TALL(-1, 1, -2, -4, 10f, 10),
    LARGE_LONG(-1, 1, -2, -4, 10f, 5),
    HUGE_TALL(-2, 2, -4, -8, 15f, 15),
    HUGE_LONG(-2, 2, -4, -8, 15f, 10),
    GARGANTUAN_TALL(-4, 4, -6, -12, 20f, 20),
    GARGANTUAN_LONG(-4, 4, -6, -12, 20f, 15),
    COLOSSAL_TALL(-8, 8, -8, -16, 30f, 30),
    COLOSSAL_LONG(-8, 8, -8, -16, 30f, 20),
    ;

    CreatureSize(int sizeModifier, int specialSizeModifier, int sizeModifierToFly, int sizeModifierToStealth, float space, int naturalReach) {
        this.sizeModifier = (byte) sizeModifier;
        this.specialSizeModifier = (byte) specialSizeModifier;
        this.sizeModifierToFly = (byte) sizeModifierToFly;
        this.sizeModifierToStealth = (byte) sizeModifierToStealth;
        this.space = space;
        this.naturalReach = (byte) naturalReach;
    }

    byte sizeModifier;
    byte specialSizeModifier;
    byte sizeModifierToFly;
    byte sizeModifierToStealth;
    float space;
    byte naturalReach;
}
