package core.enums;

public enum  Alignment {

    LAWFUL_GOOD(LawfulChaotic.LAWFUL, GoodEvil.GOOD),
    LAWFUL_NEUTRAL(LawfulChaotic.LAWFUL, GoodEvil.NEUTRAL),
    LAWFUL_EVIL(LawfulChaotic.LAWFUL, GoodEvil.EVIL),
    NEUTRAL_GOOD(LawfulChaotic.NEUTRAL, GoodEvil.GOOD),
    TRUE_NEUTRAL(LawfulChaotic.NEUTRAL, GoodEvil.NEUTRAL),
    NEUTRAL_EVIL(LawfulChaotic.NEUTRAL, GoodEvil.EVIL),
    CHAOTIC_GOOD(LawfulChaotic.CHAOTIC, GoodEvil.GOOD),
    CHAOTIC_NEUTRAL(LawfulChaotic.CHAOTIC, GoodEvil.NEUTRAL),
    CHAOTIC_EVIL(LawfulChaotic.CHAOTIC, GoodEvil.EVIL)
    ;

    private LawfulChaotic lawfulChaotic;
    private GoodEvil goodEvil;

    Alignment(LawfulChaotic lawfulChaotic, GoodEvil goodEvil) {
        this.lawfulChaotic = lawfulChaotic;
        this.goodEvil = goodEvil;
    }

    public LawfulChaotic getLawfulChaotic() {
        return lawfulChaotic;
    }

    public GoodEvil getGoodEvil() {
        return goodEvil;
    }

    public enum GoodEvil {
        GOOD,
        NEUTRAL,
        EVIL
    }

    public enum LawfulChaotic {
        LAWFUL,
        NEUTRAL,
        CHAOTIC
    }
}
